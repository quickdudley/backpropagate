import Control.Concurrent.MVar
import Data.Functor.Identity
import Test.QuickCheck
import Numeric.Backpropagate
import Numeric.Backpropagate.Matrix
import Numeric.Backpropagate.Extra
import qualified Numeric.LinearAlgebra as H

main :: IO ()
main = do
  quickCheck checkSoftmax
  quickCheck checkPolynomial

checkSoftmax :: Property
checkSoftmax = again $ \(NonEmpty l) -> ioProperty $ do
  let
    (al,yl) = foldr (\ ~(a,y) ~(l,g) -> (a:l,y:g)) ([],[]) l
    [av,yv] = map H.vector [al,yl]
  lgv <- newMVar $ zero al
  vgv <- newMVar $ zero av
  let
    lx = newDVar al $ \g -> do
      g0 <- takeMVar lgv
      putMVar lgv $ g0 `add` g
    vx = newDVar av $ \g -> do
      g0 <- takeMVar vgv
      putMVar vgv $ g0 `add` g
  backprop (softmax vx) yv
  backprop (let
    l' = map exp $ splitList lx
    in collectList $ map (/ sum l') l'
   ) yl
  ((small .) . add) <$> (map negate <$> takeMVar lgv) <*> (H.toList <$> takeMVar vgv)
 where
  -- hmatrix sometimes has different rounding errors from doing the same thing
  -- in plain Haskell
  small = all ((< 0.0000001) . abs)

checkPolynomial :: [(Double,Int)] -> Double -> Bool
checkPolynomial l' x' = let
  x = toRational x'
  l = filter (\(f,p) -> p > 1) l'
  Identity v = grad (\(Identity x_) ->
    sum $ map (\(f,p) -> x_ ^ p * auto (toRational f)) l
   ) (Identity x)
  t = sum $ map (\(f,p) -> let f' = toRational f in x ^ (p - 1) * f' * fromIntegral p) l
  in v == t
