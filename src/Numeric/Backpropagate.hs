{-# LANGUAGE CPP, DerivingVia, FlexibleContexts, FlexibleInstances,
 GeneralizedNewtypeDeriving, MultiWayIf, RankNTypes, RoleAnnotations,
 ScopedTypeVariables, StandaloneDeriving, TypeApplications, TypeFamilies,
 UndecidableInstances #-}
module Numeric.Backpropagate (
  DVar,
  Split,
  VarMonad(..),
  Gradient(..),
  backprop,
  backprop1,
  NumGradient(..),
  newDVar,
  value,
  dseq,
  gseq,
  gseq',
  promote,
  elevate,
  demote,
  auto,
  severable,
  split,
  fromSplit,
  reSplit,
  collectList,
  splitList,
  extract,
  build,
  build',
  build_,
  build'_,
  op1,
  op2,
  op3
 ) where

import Control.Concurrent.MVar
import Control.Monad.IO.Class
import Control.Monad.ST
import Control.Monad.ST.Unsafe (unsafeSTToIO)
import Data.STRef
import Data.Complex
import Data.Foldable
import Data.Functor.Const
import Data.Functor.Identity
import Data.Monoid
import Data.Proxy
import Data.Ratio
import Data.Semigroup (Min(..))
import Foreign.Storable
import qualified Numeric.LinearAlgebra as L
import System.IO.Unsafe

data DVar m a = L a Int (a -> MCascade m)
data Split m r a = S a Int (r -> MCascade m)

class (Monad m) => VarMonad m where
  type Var m :: (* -> *)
  newVar :: Proxy m -> a -> IO (Var m a)
  takeVar :: Var m a -> m a
  readVar :: Var m a -> m a
  readVar var = takeVar var >>= \val -> val <$ putVar var val
  putVar :: Var m a -> a -> m ()

instance VarMonad IO where
  type Var IO = MVar
  newVar = const newMVar
  takeVar = takeMVar
  putVar = putMVar

instance VarMonad (ST s) where
  type Var (ST s) = STRef s
  newVar = const $ unsafeSTToIO . newSTRef
  takeVar = readSTRef
  readVar = readSTRef
  putVar = writeSTRef

instance (Eq a) => Eq (DVar m a) where
  ~(L a _ _) == ~(L b _ _) = a == b

instance Ord a => Ord (DVar m a) where
  compare ~(L a _ _) ~(L b _ _) = compare a b

instance Show a => Show (DVar m a) where
  showsPrec p ~(L a _ _) = showsPrec p a

instance (VarMonad m, Gradient n, Num n) => Num (DVar m n) where
  (+) = op2 (\a b -> (a + b, \e -> (e, e)))
  (*) = op2 (\a b -> (a * b, \e -> (e * b, e * a)))
  abs = op1 (\a -> (abs a, (*) (signum a)))
  signum = op1 (\a -> (signum a, const 0))
  negate = op1 (\a -> (negate a, negate))
  (-) = op2 (\a b -> (a - b, \e -> (e, -e)))
  fromInteger = auto . fromInteger

instance (VarMonad m, Gradient n, Fractional n) => Fractional (DVar m n) where
  recip = op1 (\a -> (recip a, \e -> (-e) / (a^2)))
  (/) = op2 (\x y -> (x/y, \e -> (e/y, negate $ e*x/y^2)))
  fromRational = auto . fromRational

instance (VarMonad m, Gradient n, Floating n) => Floating (DVar m n) where
  pi = auto pi
  (**) = op2 (\x y -> (x**y, \e -> (e*y*x**(y-1), e*x**y*log x)))
  exp = op1 (\a -> let r = exp a in (r,(*r)))
  log = op1 (\a -> (log a, (/a)))
  sqrt = op1 (\a -> let r = sqrt a in (r, (/ (2 * r))))
  sin = op1 (\a -> (sin a, (* cos a)))
  cos = op1 (\a -> (cos a, negate . (* sin a)))
  asin = op1 (\a -> (asin a, (/ sqrt (1 - a^2))))
  acos = op1 (\a -> (acos a, negate . (/ sqrt (1 - a^2))))
  atan = op1 (\a -> (atan a, (/ (a^2 + 1))))
  sinh = op1 (\a -> (sinh a, (* cosh a)))
  cosh = op1 (\a -> (cosh a, (* sinh a)))
  asinh = op1 (\a -> (asinh a, (/ sqrt (a^2 + 1))))
  acosh = op1 (\a -> (acosh a, (/ sqrt (a - 1)) . (/ sqrt (a + 1))))
  atanh = op1 (\a -> (atanh a, (/ (1 - a^2))))

instance (VarMonad m, Gradient n, Real n) => Real (DVar m n) where
  toRational = toRational . value

instance (VarMonad m, Gradient n, RealFrac n) => RealFrac (DVar m n) where
  properFraction (L a l h) = let
    (b, f) = properFraction a
    in (b, L f l h)

instance (VarMonad m, Gradient n, RealFloat n) => RealFloat (DVar m n) where
  floatRadix = floatRadix . value
  floatDigits = floatDigits . value
  floatRange = floatRange . value
  decodeFloat = decodeFloat . value
  encodeFloat = (auto .) . encodeFloat
  isNaN = isNaN . value
  isInfinite = isInfinite . value
  isDenormalized = isDenormalized . value
  isNegativeZero = isNegativeZero . value
  isIEEE = isIEEE . value

backprop :: Monad m => DVar m a -> a -> m ()
backprop ~(L _ _ h) g = runCascade (h g)

backprop1 :: (Monad m, Gradient a) => DVar m a -> m ()
backprop1 l@(~(L a _ _)) = backprop l (one a)

newDVar :: forall m a . (Gradient a, VarMonad m) => a -> (a -> m ()) -> DVar m a
newDVar a h = unsafePerformIO $ do
  v <- newVar (Proxy @m) Nothing
  let
    h' = mkSkewHeap maxBound $ MCascade $ do
      getAndClear v >>= \mg -> case mg of
        Just g0 -> mempty <$ h g0
        Nothing -> return mempty
  return $ L a maxBound (\g -> MCascade $ do
    addWithAdd v g
    return h'
   )

auto :: Applicative m => a -> DVar m a
auto a = L a maxBound mempty

value :: DVar m a -> a
value ~(L a _ _) = a

dseq :: DVar m a -> b -> b
dseq (L a d _) b = d `seq` a `seq` b

gseq :: DVar m a -> DVar m a
gseq ~(L a d h) = L a d (\g -> g `seq` h g)

gseq' :: (a -> ()) -> DVar m a -> DVar m a
gseq' e ~(L a d h) = L a d (\g -> e g `seq` h g)

promote :: DVar m a -> DVar m (DVar m a)
promote a@(~(L v d h)) = L a d (h . value)

elevate :: Applicative m => DVar m a -> DVar m (DVar m a)
elevate ~(L v d h) = L (auto v) d (h . value)

demote :: Applicative m => DVar m (DVar m a) -> DVar m a
demote ~(L ~(L v d1 h1) d2 h2) = L v (min d1 d2) (\g -> h1 g <> h2 (auto g))

severable :: forall m a io . (MonadIO io, VarMonad m) => DVar m a -> io (m (), DVar m a)
severable ~(L v _ h) = liftIO $ do
  hv <- newVar (Proxy @m) h
  return (takeVar hv >> putVar hv (const mempty), L v (maxBound - 1) $ \g -> MCascade $ do
    h' <- readVar hv
    let MCascade z = h' g in z
   )

split :: (VarMonad m) => DVar m a -> (r -> r -> m r) -> (r -> m a) -> Split m r a
split ~(L a d h) cf rf = reSplit (S a d h) cf rf

fromSplit :: Monad m => Split m r a -> (a -> b) -> (b -> m r) -> DVar m b
fromSplit ~(S v d h) vf rf = L (vf v) d (\g -> MCascade $ do
  z <- rf g
  let MCascade r = h z
  r
 )

reSplit :: forall m s r a . (VarMonad m) => Split m r a -> (s -> s -> m s) -> (s -> m r) -> Split m s a
reSplit ~(S a d h) cf rf = unsafePerformIO $ do
  gv <- newVar (Proxy @m) Nothing
  let
    d1 = d - 1
  return $ S (d1 `seq` a) d1 $ \g -> MCascade $ do
    addWith gv cf g
    return $ mkSkewHeap d1 $ MCascade $ getAndClear gv >>= \mg -> case mg of
      Nothing -> return mempty
      Just gr -> rf gr >>= \b -> let MCascade r = h b in r

build :: forall m r . (VarMonad m, Gradient r) => (forall bm . (Monad bm) =>
  (forall s . DVar m s -> (s -> r -> m s) -> bm s) ->
  bm r
 ) -> DVar m r
build df = let
  cap ~(L v d h) z = let
    z' = z v
    in ((Min d, \r -> MCascade $ z' r >>= \g -> let MCascade f = h g in f), v)
  ((Min d', h'), v') = df cap
  in unsafePerformIO $ do
    let d'' = d' - 1
    b <- newVar (Proxy @m) Nothing
    let
      h'' = \g -> MCascade $ do
        addWithAdd b g
        return $ mkSkewHeap d'' $ MCascade $ do
          getAndClear b >>= \mg -> case mg of
            Just g0 -> let MCascade f = h' g0 in f
            Nothing -> return mempty
    return $ L (d'' `seq` v') d'' h''

build_ :: forall m r . (VarMonad m, Gradient r) => (
  forall bm t . (Monad bm, Monoid t) =>
  (forall s . DVar m s -> bm (s, s -> m t)) ->
  bm (r, r -> m t)
 ) -> DVar m r
build_ df = let
  cap ~(L v d h) = (Min d, (v, return . h))
  (Min d', (v', h')) = df cap
  in unsafePerformIO $ do
    let d'' = d' - 1
    b <- newVar (Proxy @m) Nothing
    let
      h'' = \g -> MCascade $ do
        addWithAdd b g
        return $ mkSkewHeap d'' $ MCascade $ do
          getAndClear b >>= \mg -> case mg of
            Just g0 -> h' g0 >>= \(MCascade f) -> f
            Nothing -> return mempty
    return $ L (d'' `seq` v') d'' h''

build' :: forall m r . Monad m => (forall bm . Monad bm =>
  (forall s . DVar m s -> (s -> r -> m s) -> bm s) ->
  bm r
 ) -> DVar m r
build' df = let
  cap ~(L v d h) z = let
    z' = z v
    in ((Min d, \r -> MCascade $ z' r >>= \g -> let MCascade f = h g in f), v)
  ((Min d', h'), v') = df cap
  in L (d' `seq` v') d' h'

build'_ :: forall m r . (Monad m) => (
  forall bm t . (Monad bm, Monoid t) =>
  (forall s . DVar m s -> bm (s, s -> m t)) ->
  bm (r, r -> m t)
 ) -> DVar m r
build'_ df = let
  cap ~(L v d h) = (Min d, (v, return . h))
  (Min d', (v', h')) = df cap
  d'' = d' - 1
  in L (d'' `seq` v') d'' (\g -> MCascade $ do
    MCascade h'' <- h' g
    h''
   )

addWith :: VarMonad m => Var m (Maybe a) -> (a -> a -> m a) -> a -> m ()
addWith var c a = takeVar var >>= \m -> case m of
  Just a0 -> do
    x <- c a0 a
    putVar var $ Just x
  Nothing -> putVar var $ Just a

addWithAdd :: (VarMonad m, Gradient a) => Var m (Maybe a) -> a -> m ()
addWithAdd var = addWith var (\a b -> return $ add a b)

getAndClear :: VarMonad m => Var m (Maybe a) -> m (Maybe a)
getAndClear var = do
  a <- takeVar var
  putVar var Nothing
  return a

op1 :: (VarMonad m, Gradient b) => (a -> (b, b -> a)) -> DVar m a -> DVar m b
op1 f a' = build_ (\cap -> do
  (a, as) <- cap a'
  let (b, rg) = f a
  return (b, as . rg)
 )

op2 :: (VarMonad m, Gradient c) => (a -> b -> (c, c -> (a, b))) -> DVar m a -> DVar m b -> DVar m c
op2 f a' b' = build_ (\cap -> do
  (a, as) <- cap a'
  (b, bs) <- cap b'
  let (c, rg) = f a b
  return (c, \g -> let (ag,bg) = rg g in (<>) <$> as ag <*> bs bg)
 )

op3 :: (VarMonad m, Gradient d) => (a -> b -> c -> (d, d -> (a, b, c))) ->
  DVar m a -> DVar m b -> DVar m c -> DVar m d
op3 f a' b' c' = build_ (\cap -> do
  (a, as) <- cap a'
  (b, bs) <- cap b'
  (c, cs) <- cap c'
  let (d, rg) = f a b c
  return (d, \g -> let
    (ag,bg,cg) = rg g
    in (<>) <$> as ag <*> ((<>) <$> bs bg <*> cs cg)
   )
 )

instance Functor (Split m r) where
  fmap f ~(S v d h) = S (f v) d h

collectList :: Applicative m => [DVar m a] -> DVar m [a]
collectList [] = auto []
collectList l = let
  lp = minimum $ map (\(L _ d _) -> d) l
  v = map (\(L v' _ _) -> v') l
  h = fold . zipWith ($) (map (\(L _ _ h') -> h') l)
  in L v lp h

splitList :: (VarMonad m, Gradient a) => DVar m [a] -> [DVar m a]
splitList l = let
  s = split l (\a b -> return $ merge a b) (return . aig 0)
  merge [] b = b
  merge a [] = a
  merge al@(a@(ai,av):ar) bl@(b@(bi,bv):br) = case ai `compare` bi of
    LT -> a : merge ar bl
    EQ -> merge ((ai, add av bv):ar) bl
    GT -> b : merge al br
  aig _ [] = []
  aig i a@((j,v):r)
    | i == j = v : aig (i + 1) r
    | otherwise = zero v : aig (i + 1) a
  in zipWith (\x i -> fromSplit s (const x) (\g -> return [(i,g)])) (value l) [0 ..]

extract :: (Gradient a) =>
  DVar m a -> (forall f . Functor f => (b -> f b) -> a -> f a) ->
  DVar m b
extract (L v d h) l = let
  d' = d - 1
  Const b = l Const v
  z = zero v
  h' g = let
    Identity g' = l (const $ Identity g) z
    in h g'
  in L b d' h'

class Gradient a where
  zero :: a -> a
  one :: a -> a
  add :: a -> a -> a

newtype NumGradient n = NumGradient n deriving (Ord, Eq, Num, Fractional, Floating, Real)

instance (Num a, Eq a) => Gradient (NumGradient a) where
  zero = const 0
  one = const 1
  add = (+)

deriving via (NumGradient Integer)
  instance Gradient Integer

deriving via (NumGradient Int)
  instance Gradient Int

deriving via (NumGradient Word)
  instance Gradient Word

deriving via (NumGradient Float)
  instance Gradient Float

deriving via (NumGradient Double)
  instance Gradient Double

deriving via (NumGradient (Ratio n))
  instance Integral n => Gradient (Ratio n)

deriving via (NumGradient (Complex n))
  instance RealFloat n => Gradient (Complex n)

deriving via (NumGradient (L.Matrix Double))
  instance Gradient (L.Matrix Double)
deriving via (NumGradient (L.Matrix Float))
  instance Gradient (L.Matrix Float)
deriving via (NumGradient (L.Matrix (Complex Double)))
  instance Gradient (L.Matrix (Complex Double))
deriving via (NumGradient (L.Matrix (Complex Float)))
  instance Gradient (L.Matrix (Complex Float))
deriving via (NumGradient (L.Matrix L.Z))
  instance Gradient (L.Matrix L.Z)
deriving via (NumGradient (L.Matrix L.I))
  instance Gradient (L.Matrix L.I)

instance (L.Container L.Vector n, Num n, Eq n, Num (L.Vector n)) => Gradient (L.Vector n) where
  zero = const 0
  one = L.cmap (const 1)
  add a b = let
    l = maximum $ map L.size [a,b]
    pad v' = let
      l' = L.size v'
      in if
        | l' == 1 -> v'
        | l' < l -> L.vjoin [v', L.fromList $ replicate (l - l') 0]
        | otherwise -> v'
    in pad a + pad b

instance (VarMonad m, Gradient n) => Gradient (DVar m n) where
  zero = op1 (\a -> (zero a, zero))
  one = op1 (\a -> (one a, zero))
  add = op2 (\a b -> (add a b, \e -> (e,e)))

instance Gradient a => Gradient [a] where
  zero = const []
  one = map one
  add a [] = a
  add [] b = b
  add (a:ar) (b:br) = add a b : add ar br

instance (Gradient a, Gradient b) => Gradient (a,b) where
  zero ~(a,b) = (zero a, zero b)
  one ~(a,b) = (one a, one b)
  add ~(a1,b1) ~(a2,b2) = (add a1 a2, add b1 b2)

instance (Gradient a, Gradient b, Gradient c) => Gradient (a,b,c) where
  zero ~(a,b,c) = (zero a, zero b, zero c)
  one ~(a,b,c) = (one a, one b, one c)
  add ~(a1,b1,c1) ~(a2,b2,c2) = (add a1 a2, add b1 b2, add c1 c2)

instance (Gradient a, Gradient b, Gradient c, Gradient d) => Gradient (a,b,c,d) where
  zero ~(a,b,c,d) = (zero a, zero b, zero c, zero d)
  one ~(a,b,c,d) = (one a, one b, one c, one d)
  add ~(a1,b1,c1,d1) ~(a2,b2,c2,d2) = (add a1 a2, add b1 b2, add c1 c2, add d1 d2)

newtype MCascade m = MCascade (m (SkewHeap Int (MCascade m)))
type IOCascade = MCascade IO

deriving via (Ap m (SkewHeap Int (MCascade m)))
  instance (Applicative m) => Semigroup (MCascade m)

deriving via (Ap m (SkewHeap Int (MCascade m)))
  instance (Applicative m) => Monoid (MCascade m)

runCascade :: Monad m => MCascade m -> m ()
runCascade = go SKTip where
  go q (MCascade a) = a >>= \r -> case pop (q <> r) of
    Just (_,n,s) -> go s n
    Nothing -> return ()

#if __GLASGOW_HASKELL__ >= 708
type role SkewHeap nominal representational
#endif

data SkewHeap k a = SKTip | SKNode k a (SkewHeap k a) (SkewHeap k a)

instance Ord k => Semigroup (SkewHeap k a) where
  SKTip <> b = b
  a <> SKTip = a
  a@(SKNode k1 v1 l1 r1) <> b@(SKNode k2 v2 l2 r2)
    | k1 <= k2 = SKNode k1 v1 (b <> r1) l1
    | otherwise = SKNode k2 v2 (a <> r2) l2

instance Ord k => Monoid (SkewHeap k a) where
  mempty = SKTip

pop :: Ord k => SkewHeap k a -> Maybe (k,a,SkewHeap k a)
pop SKTip = Nothing
pop (SKNode k v l r) = Just (k, v, l <> r)

mkSkewHeap :: Ord k => k -> a -> SkewHeap k a
mkSkewHeap k a = SKNode k a SKTip SKTip

