{-# LANGUAGE RankNTypes #-}
module Numeric.Backpropagate.Extra where
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.ST
import Data.IORef
import Data.STRef
import Numeric.Backpropagate

adversary :: (Num a, VarMonad m, Gradient a) => DVar m a -> DVar m a
adversary = op1 (\n -> (n, negate))

-- | Creates an 'IO' action which yields a (possibly different) differentiable
-- value each time it is run. The gradient handler for each yielded 'DVar'
-- value does the following:
--
-- 1. Adds the gradient to the value yielded by subsequent invocations of the 'IO' action
-- 2. Passes the new value and the gradient to the second argment.
accumulateAndTrack :: (Gradient a, MonadIO m, VarMonad m) => a -> (a -> a -> m ()) -> IO (IO (DVar m a))
accumulateAndTrack a h0 = do
  v <- newIORef a
  let
    h = \g -> join $
      liftIO $ atomicModifyIORef v (\c -> let
        s = add c g
        in (s, h0 g s)
       )
  return $ do
    c <- readIORef v
    return $ newDVar c h

accumulator :: (Gradient a, MonadIO m, VarMonad m) => a -> IO (IO (DVar m a))
accumulator a = do
  v <- newIORef a
  let
    h = \g -> liftIO $ atomicModifyIORef v (\c -> (add c g,()))
  return $ do
    c <- readIORef v
    return $ newDVar c h

grad :: (Traversable t, Gradient a, Gradient b) => (forall s . t (DVar (ST s) a) -> DVar (ST s) b) -> t a -> t a
grad f x = runST $ do
  p <- traverse (\x -> do
    g' <- newSTRef (zero x)
    return (g', newDVar x (modifySTRef g' . add))
   ) x
  backprop1 $ f $ snd <$> p
  traverse (readSTRef . fst) p

