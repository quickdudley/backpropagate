{-# LANGUAGE FlexibleContexts, ScopedTypeVariables, TypeFamilies, UndecidableInstances #-}
module Numeric.Backpropagate.Matrix (
  Matrix(..),
  Container(..),
  Flat(..),
  relu,
  sigmoid,
  softmax,
  svd,
  vectorToList,
  atIndices,
  listToVector,
  Numeric.Backpropagate.Matrix.splitAt,
  crossEntropy,
  clipScaleGradient
 ) where

import Data.Foldable
import Data.Functor.Compose
import Data.List
import Data.Monoid (Ap(..))
import Foreign.Storable
import qualified Numeric.LinearAlgebra as H
import Numeric.Backpropagate
import Prelude hiding ((<>))
import qualified Prelude ((<>))

class Matrix (m :: *) where
  type Vector m :: *
  infixr 8 #>
  (#>) :: m -> Vector m -> Vector m
  infixr 8 <#
  (<#) :: Vector m -> m -> Vector m
  infixr 8 <>
  (<>) :: m -> m -> m
  tr :: m -> m
  outer :: Vector m -> Vector m -> m
  cols :: m -> Int
  rows :: m -> Int
  reshape :: Int -> Vector m -> m
  flatten :: m -> Vector m
  subMatrix :: (Int,Int) -> (Int,Int) -> m -> m
  fromBlocks :: [[m]] -> m

instance (H.Numeric t, Eq t, Num (H.Vector t)) => Matrix (H.Matrix t) where
  type Vector (H.Matrix t) = H.Vector t
  (#>) = (H.#>)
  (<#) = (H.<#)
  (<>) = (H.<>)
  tr = H.tr
  outer = H.outer
  cols = H.cols
  rows = H.rows
  reshape = H.reshape
  flatten = H.flatten
  subMatrix = H.subMatrix
  fromBlocks = H.fromBlocks

instance (Matrix u, VarMonad m, Gradient u, Gradient (Vector u), Container u, Index u ~ (Int,Int), Num (Element u)) => Matrix (DVar m u) where
  type Vector (DVar m u) = DVar m (Vector u)
  (#>) = op2 (\a b -> (a #> b, \d -> (d `outer` b, tr a #> d)))
  (<#) = op2 (\a b -> (a <# b, \d -> (d <# tr b, a `outer` d)))
  (<>) = op2 (\a b -> (a <> b, \d -> (d <> tr b, tr a <> d)))
  tr = op1 (\a -> (tr a, tr))
  outer = op2 (\a b -> (a `outer` b, \d -> (d #> b, a <# d)))
  cols = cols . value
  rows = rows . value
  reshape w = op1 $ \v -> (reshape w v, flatten)
  flatten = op1 $ \m -> let
    w = cols m
    in (flatten m, reshape w)
  subMatrix b@(y0,x0) s@(yn,xn) = op1 $ \v -> let
    [yr,xr] = zipWith3 (\s0 sn f -> f v - s0 - sn) [y0,x0] [yn,xn] [rows,cols]
    lu = clean b
    mu = clean (y0,xn)
    ru = clean (y0,xr)
    lm = clean (yn,x0)
    mmz = clean s
    rm = clean (yn,xr)
    lb = clean (yr,x0)
    mb = clean (yr,xn)
    rb = clean (yr,xr)
    in (
      subMatrix b s v,
      \g -> fromBlocks [[lu,mu,ru],[lm, add mmz g, rm], [lb,mb,rb]]
     )
  fromBlocks l2 = let
    heights = map (maximum . map rows) l2
    widths = map (maximum . map cols) $ transpose l2
    [xs,ys] = map (snd . mapAccumL (\s v -> let r = s + v in (r,r)) 0) [widths, heights]
    in build_ (\cap -> do
      z <- traverse cap (Compose l2)
      let
        Compose e = fmap fst z
        Compose h = fmap snd z
      return (fromBlocks e, \g -> getAp $ fold $ zipWith3 (\y0 yn row ->
        fold $ zipWith3 (\x0 xn h -> Ap $ h (subMatrix (y0,x0) (yn,xn) g)) (0 : xs) widths row
       ) (0 : ys) heights h)
     )

class Container c where
  type Element c :: *
  type Index c :: *
  size :: c -> Index c
  sumElements :: c -> Element c
  scalar :: Element c -> c
  fill :: c -> Element c -> c
  accum :: c -> [(Index c, Element c)] -> c
  atIndex :: c -> Index c -> Element c
  konst :: Element c -> Index c -> c

clean :: (Num (Element c), Container c) => Index c -> c
clean = konst 0

instance (H.Container H.Vector e, Gradient e) => Container (H.Vector e) where
  type Element (H.Vector e) = e
  type Index (H.Vector e) = H.IndexOf H.Vector
  size = H.size
  sumElements = H.sumElements
  scalar = H.scalar
  fill c e = H.konst e (size c)
  accum = flip H.accum add
  atIndex = H.atIndex
  konst = H.konst

instance (H.Container H.Matrix e, H.Container H.Vector e, Gradient e, Num e) => Container (H.Matrix e) where
  type Element (H.Matrix e) = e
  type Index (H.Matrix e) = H.IndexOf H.Matrix
  size = H.size
  sumElements = H.sumElements
  scalar = H.scalar
  fill c e = H.konst e (size c)
  accum = flip H.accum add
  atIndex = H.atIndex
  konst = H.konst

instance (Container c, VarMonad m, Gradient c, Gradient (Element c), Num (Element c)) => Container (DVar m c) where
  type Element (DVar m c) = DVar m (Element c)
  type Index (DVar m c) = Index c
  size = size . value
  sumElements = op1 (\v -> (sumElements v, fill v))
  scalar = op1 (\v -> (scalar v, sumElements))
  fill c = let
    c' = value c
    in op1 (\e -> (fill c' e, sumElements))
  accum s l = build_ $ \cap -> do
    (s', sg) <- cap s
    Compose l'' <- traverse cap (Compose l)
    let
      (l',lgs) = foldr (\ ~(i,(e,g)) ~(er,gr) -> ((i,e):er,(i,g):gr)) ([],[]) l''
    return (accum s' l', \g -> getAp $ Ap (sg g) Prelude.<>
      foldMap (\(i,h) -> Ap (h $ g `atIndex` i)) lgs
     )
  atIndex c = let
    s = split c (\a b -> return (a . b)) (\acc -> return $
      accum (fill (value c) 0) (acc [])
     )
    in \i -> fromSplit s (flip atIndex i) (\g -> return ((i,g):))
  konst k s = op1 (\k' -> (konst k' s, sumElements)) k

class Container c => Flat c where
  vjoin :: [c] -> c
  takesV :: [Int] -> c -> [c]

instance (Storable e, H.Container H.Vector e, Gradient e) => Flat (H.Vector e) where
  vjoin = H.vjoin
  takesV = H.takesV

instance (Flat c, VarMonad m, Index c ~ Int, Gradient c, Gradient (Element c), Num (Element c), Num c) => Flat (DVar m c) where
  vjoin l = build_ $ \cap -> do
    l' <- traverse cap l
    let
      (ss, vs, gs) = foldr (\ ~(v,g) ~(sr,vr,gr) -> (size v:sr,v:vr,g:gr)) ([],[],[]) l'
    return (vjoin vs, \g -> getAp $ fold $ zipWith ($) (map (Ap .) gs) $ takesV ss g)
  takesV ss v = let
    s = split v ((return . ) . merge) (return . vjoin . rb 0 ss)
    merge [] b = b
    merge a [] = a
    merge al@(a@(ai,ag):ar) bl@(b@(bi,bg):br) = case ai `compare` bi of
      LT -> a : merge ar bl
      EQ -> merge ((ai, add ag bg):ar) br
      GT -> b : merge al br
    rb _ [] _ = []
    rb ci (cs:sr) ((gi,g):gr)
      | gi == ci = g + konst 0 cs : rb (ci + 1) sr gr
    rb ci (cs:sr) gs = konst 0 cs : rb (ci + 1) sr gs
    in zipWith
      (\sv i -> fromSplit s (const sv) (return . return . (,) i))
      (takesV ss (value v))
      [0 ..]

relu :: (VarMonad m, H.Container c e, Num e, Ord e, Num (c e), Gradient (c e)) => DVar m (c e) -> DVar m (c e)
relu = op1 $ \a -> (H.cmap (max 0) a, \g -> H.cond a 0 0 g g)

sigmoid :: (Floating n, Gradient n, VarMonad m) => DVar m n -> DVar m n
sigmoid = op1 $ \a -> let r = 1 / (1 + exp(-a)) in (r, (* (1 - r)) . (* r))

softmax :: (Floating c, Container c) => c -> c
softmax x = let
  e = exp x
  in e / scalar (sumElements e)

svd :: VarMonad m => DVar m (H.Matrix Double) -> (H.Matrix Double, DVar m (H.Vector Double), H.Matrix Double)
svd m = let
  (u,s,v) = H.svd $ value m
  in (u, build_ (\cap -> do
    (_,ms) <- cap m
    return (s, \g -> ms $ (u H.<> H.diag g) H.<> H.tr v)
   ), v)

vectorToList :: (Container c, Num (Index c), Enum (Index c)) => c -> [Element c]
vectorToList v = atIndices v [0 .. size v - 1]

atIndices :: (Functor f, Container c) => c -> f (Index c) -> f (Element c)
atIndices v ixs = fmap (atIndex v) ixs

listToVector :: (VarMonad m, H.Container H.Vector e, Eq e, Num e, Num (H.Vector e)) =>
  [DVar m e] -> DVar m (H.Vector e)
listToVector l = build_ (\cap -> do
  z <- traverse cap l
  let
    e = map fst z
    h = map snd z
  return (H.fromList e, \g -> getAp $ fold $ zipWith (\i h -> Ap $
    h $ g `H.atIndex` i
   ) [0 ..] h)
 )

splitAt :: (VarMonad m, H.Element e, H.Container H.Vector e, Gradient (H.Vector e), Num e, Num (H.Vector e)) =>
  Int -> DVar m (H.Vector e) -> (DVar m (H.Vector e), DVar m (H.Vector e))
splitAt i v = let
  l = H.size $ value v
  s = split v
    (\ ~(a0,b0) ~(a1,b1) -> return (add a0 b0, add a1 b1))
    (\ ~(a,b) -> return $ H.vjoin [checkSize i a, checkSize (l - i) b])
  checkSize l' a = if H.size a < l'
    then a + H.build l' (const 0)
    else a
  in (
    fromSplit s (H.subVector 0 i) (\g -> return (g, H.scalar 0)),
    fromSplit s (H.subVector i (l - i)) (\g -> return (H.scalar 0,g))
   )

crossEntropy :: Floating n => [n] -> [n] -> n
crossEntropy t m = sum $ zipWith (\t' m' ->
  -(t' * log m' + (1 - t') * log (1 - t'))
 ) t m

clipScaleGradient :: (VarMonad m, Ord e, Fractional e, H.Container H.Vector e, Num (H.Vector e))
  => e -> DVar m (H.Vector e) -> DVar m (H.Vector e)
clipScaleGradient lim = op1 (\v -> (v, \g -> let
  h = H.maxElement g
  in if h > lim
   then g * H.scalar (lim / h)
   else g
 ))
